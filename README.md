Introduction

A boilerplate project for Android

Step 1:
Download or clone this repo by using the link below:


Step 2: Dependencies Used

1.Retrofit and Gson converter
2.Life-cycle and View model component
3.Dragger


Step 3 : Features
1. MVVM : Data Binding
2.Generic API structure.
3.Generic Folder structure.
4.Commonly used functions

Step 4 :  Conclusion

This boilerplate project is based on  Kotlin with MVVM designing pattern. Supported on  android devices.
Activity and fragments are managed along with callbacks,proper life cycle of activity and fragment has been managed into it along with API Integration.Also common functions are added into it.