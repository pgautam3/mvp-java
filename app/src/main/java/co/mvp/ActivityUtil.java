package co.mvp;

import android.content.Context;
import android.content.Intent;

import co.mvp.demoMVP.newHome.HomeNewActivity;
import co.mvp.forgotPassword.ForgotPassswordActivity;
import co.mvp.registartion.RegistrationActivity;

public class ActivityUtil {
  private Context context;

  public ActivityUtil(Context context) {
    this.context = context;
  }
  public void startForgotPasswowdActivity() {
    context.startActivity(new Intent(context, ForgotPassswordActivity.class));
  }
  public void startMainActivity() {
    context.startActivity(new Intent(context, MainActivity.class));
  }

  public void startregistrationActivity() {
    context.startActivity(new Intent(context, RegistrationActivity.class));
  }
  public void startRecyclerViewActivity() {
    context.startActivity(new Intent(context, HomeNewActivity.class));
  }
}
