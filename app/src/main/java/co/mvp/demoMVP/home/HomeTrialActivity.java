package co.mvp.demoMVP.home;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.List;

import co.mvp.R;
import co.mvp.demoMVP.newHome.AndroidPresenter;
import co.mvp.demoMVP.newHome.CountryAdapter;
import co.mvp.forgotPassword.ForgotPasswordPresenter;
import co.mvp.forgotPassword.ForgotPasswordService;

public class HomeTrialActivity extends AppCompatActivity implements HomeView {
    private HomePresenter presenter;
    RecyclerView recyclerView;
    LinearLayoutManager linearLayoutManager;
    CountryAdapter countryAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_trial);
        presenter = new HomePresenter(this, new HomeService());

    }
    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public List<String> getTodos(String user) {
        return null;
    }
}