package co.mvp.demoMVP.home;

import java.util.List;

public interface ToDoService {
    public List<String> getTodos(String user);
}
