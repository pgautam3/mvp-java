package co.mvp.demoMVP.home;

import java.util.List;

public interface HomeView {
    public List<String> getTodos(String user);

}
