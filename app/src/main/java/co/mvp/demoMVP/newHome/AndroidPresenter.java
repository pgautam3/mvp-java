package co.mvp.demoMVP.newHome;

import android.content.Context;

import java.util.List;


public class AndroidPresenter implements AndroidGetDataContract.Presenter, AndroidGetDataContract.onGetDataListener {
    private AndroidGetDataContract.View mGetDataView;
    private AndroidIntractor mIntractor;
    public AndroidPresenter(AndroidGetDataContract.View mGetDataView){
        this.mGetDataView = mGetDataView;
        mIntractor = new AndroidIntractor(this);
    }
    @Override
    public void getDataFromURL(Context context, String url) {
        mIntractor.initRetrofitCall(context,url);
    }

    @Override
    public void onSuccess(String message, List<AndroidVersion> allCountriesData) {
        mGetDataView.onGetDataSuccess(message, allCountriesData);
    }

    @Override
    public void onFailure(String message) {
        mGetDataView.onGetDataFailure(message);
    }
}
