package co.mvp.demoMVP.newHome;

import android.content.Context;

import java.util.List;


public class AndroidGetDataContract {
    interface View{
        void onGetDataSuccess(String message, List<AndroidVersion> list);
        void onGetDataFailure(String message);
    }
    interface Presenter{
        void getDataFromURL(Context context, String url);
    }
    interface Interactor{
        void initRetrofitCall(Context context, String url);

    }
    interface onGetDataListener{
        void onSuccess(String message, List<AndroidVersion> list);
        void onFailure(String message);
    }
}
