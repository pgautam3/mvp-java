package co.mvp.demoMVP.newHome;

import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface RequestInterface {

    @GET("android/jsonandroid")
   // Call<JSONResponse> getJSON(HashMap<String, String> hm);
    Call<JSONResponse> getJSON();

    @GET("android/jsonandroid")
    Call<ResponseBody> client_service(@QueryMap HashMap<String, String> hm);

}