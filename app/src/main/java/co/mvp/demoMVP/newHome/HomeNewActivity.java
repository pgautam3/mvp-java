package co.mvp.demoMVP.newHome;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import java.util.List;

import co.mvp.R;


public class HomeNewActivity extends AppCompatActivity implements AndroidGetDataContract.View {
    private AndroidPresenter mPresenter;
    RecyclerView recyclerView;
    LinearLayoutManager linearLayoutManager;
    CountryAdapter countryAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        mPresenter = new AndroidPresenter(this);
        mPresenter.getDataFromURL(getApplicationContext(), "");
        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
    }
    @Override
    public void onGetDataSuccess(String message, List<AndroidVersion> list) {
        Log.e("LIST", String.valueOf(list.size()));
        countryAdapter = new CountryAdapter(getApplicationContext(), list);
        recyclerView.setAdapter(countryAdapter);
    }

    @Override
    public void onGetDataFailure(String message) {
    }
}
