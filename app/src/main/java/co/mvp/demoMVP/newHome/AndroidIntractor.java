package co.mvp.demoMVP.newHome;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AndroidIntractor implements AndroidGetDataContract.Interactor {
    private AndroidGetDataContract.onGetDataListener mOnGetDatalistener;
    List<AndroidVersion> allcountry = new ArrayList<>();
    List<String> allCountriesData = new ArrayList<>();
    private ArrayList<AndroidVersion> data;

    public AndroidIntractor(AndroidGetDataContract.onGetDataListener mOnGetDatalistener) {
        this.mOnGetDatalistener = mOnGetDatalistener;
    }

    @Override
    public void initRetrofitCall(Context context, String url) {
        HashMap<String, String> hm = new HashMap<>();
        RequestInterface apiService = ApiClient.getClient().create(RequestInterface.class);
        apiService.client_service(hm).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.e("Android INterfaceCHECKING,,,,,,,,,,,,", String.valueOf(response));
                try {
                    if (response.code() == 200) {
                        String str = response.body().string();
                        JSONObject jsonObject = new JSONObject(str);
                        List<AndroidVersion> items = new Gson().fromJson(String.valueOf(jsonObject.getJSONArray("android")), new TypeToken<List<AndroidVersion>>() {
                        }.getType());
                        allcountry.addAll(items);
                        for (int i = 0; i < allcountry.size(); i++) {
                            allCountriesData.add(allcountry.get(i).getName());
                        }
                        mOnGetDatalistener.onSuccess("List Size: " + allCountriesData.size(), allcountry);

                    } else if (response.code() == 400) {
                    } else {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mOnGetDatalistener.onFailure(t.getMessage());

            }
        });
    }
}



