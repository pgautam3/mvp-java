package co.mvp.forgotPassword;

public interface ForgotPasswordView {
    String getUsername();
    void showUsernameError(int resId);
    void startMainActivity();
    void showLoginError(int resId);
}
