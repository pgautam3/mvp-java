package co.mvp.forgotPassword;

import android.content.Context;
import android.content.Intent;

import co.mvp.LoginActivity;
import co.mvp.MainActivity;

public class FPActivityUtil {
    private Context context;

    public FPActivityUtil(Context context) {
        this.context = context;
    }

    public void startMainActivity() {
        context.startActivity(new Intent(context, LoginActivity.class));
    }
}
