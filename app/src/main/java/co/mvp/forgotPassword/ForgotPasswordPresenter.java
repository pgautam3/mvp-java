package co.mvp.forgotPassword;

import co.mvp.LoginService;
import co.mvp.LoginView;
import co.mvp.R;

public class ForgotPasswordPresenter {
    private ForgotPasswordView view;
    private ForgotPasswordService service;

    public ForgotPasswordPresenter(ForgotPasswordView view, ForgotPasswordService service) {
        this.view = view;
        this.service = service;
    }

    public void onFPClicked() {
        String username = view.getUsername();
        if (username.isEmpty()) {
            view.showUsernameError(R.string.username_error);
            return;
        }

        boolean fpSucceeded = service.ForgotPassword(username);
        if (fpSucceeded) {
            view.startMainActivity();
            return;
        }
        view.showLoginError(R.string.fp_failed);
    }
}

