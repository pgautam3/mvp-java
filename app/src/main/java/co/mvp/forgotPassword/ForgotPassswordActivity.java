package co.mvp.forgotPassword;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import co.mvp.LoginPresenter;
import co.mvp.LoginService;
import co.mvp.LoginView;
import co.mvp.R;

public class ForgotPassswordActivity extends AppCompatActivity implements ForgotPasswordView {
    private EditText usernameView;
    private EditText passwordView;
    private ForgotPasswordPresenter presenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_passsword);
        usernameView = (EditText) findViewById(R.id.username);
        presenter = new ForgotPasswordPresenter(this, new ForgotPasswordService());

    }
    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public void onForgotPasswordClicked(View view) {
        presenter.onFPClicked();
    }

    @Override
    public String getUsername() {
        return usernameView.getText().toString();
    }

    @Override
    public void showUsernameError(int resId) {
        usernameView.setError(getString(resId));
    }


    @Override
    public void startMainActivity() {
        new FPActivityUtil(this).startMainActivity();
    }

    @Override
    public void showLoginError(int resId) {
        Toast.makeText(this, getString(resId), Toast.LENGTH_SHORT).show();

    }
}