package co.mvp.registartion;

import android.widget.EditText;

public interface RegistrationView {
    String getName();
    String getDOB();
    String getMobile();
    String getUsername();
    String getPassword();

    void showNameError(int resId);
    void showDOBError(int resId);
    void showMobileError(int resId);
    void showUsernameError(int resId);
    void showPasswordError(int resId);
    void startMainActivity();
    void showRegError(int resId);
}

