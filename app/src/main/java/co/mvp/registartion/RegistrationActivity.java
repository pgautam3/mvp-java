package co.mvp.registartion;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import co.mvp.ActivityUtil;
import co.mvp.LoginPresenter;
import co.mvp.LoginView;
import co.mvp.R;

public class RegistrationActivity extends AppCompatActivity implements RegistrationView {
    private EditText name;
    private EditText dob;
    private EditText username;
    private EditText password;
    private EditText mobile;
    private RegistrationPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        name = (EditText) findViewById(R.id.name);
        dob = (EditText) findViewById(R.id.dob);
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        mobile = (EditText) findViewById(R.id.mobile);
        presenter = new RegistrationPresenter(this, new RegistrationService());

    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    public void onRegClicked(View view) {
        presenter.onRegistrationClicked();
    }


    @Override
    public String getName() {
        return name.getText().toString();
    }


    @Override
    public String getDOB() {
        return dob.getText().toString();
    }

    @Override
    public String getMobile() {
        return mobile.getText().toString();
    }

    @Override
    public String getUsername() {
        return username.getText().toString();
    }

    @Override
    public String getPassword() {
        return password.getText().toString();
    }

    @Override
    public void showNameError(int resId) {
        name.setError(getString(resId));
    }

    @Override
    public void showDOBError(int resId) {
        dob.setError(getString(resId));


    }

    @Override
    public void showMobileError(int resId) {
        mobile.setError(getString(resId));

    }

    @Override
    public void showUsernameError(int resId) {
        username.setError(getString(resId));

    }

    @Override
    public void showPasswordError(int resId) {
        password.setError(getString(resId));

    }

    @Override
    public void startMainActivity() {
        new RegistrtaionActivityUtil(this).startMainActivity();

    }

    @Override
    public void showRegError(int resId) {
        Toast.makeText(this, getString(resId), Toast.LENGTH_SHORT).show();


    }
}