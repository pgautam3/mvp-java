package co.mvp.registartion;

import co.mvp.LoginService;
import co.mvp.LoginView;
import co.mvp.R;
import co.mvp.forgotPassword.ForgotPasswordService;
import co.mvp.forgotPassword.ForgotPasswordView;

public class RegistrationPresenter {
    private RegistrationView view;
    private RegistrationService service;


    public RegistrationPresenter(RegistrationView view, RegistrationService service) {
        this.view = view;
        this.service = service;
    }


    public void onRegistrationClicked() {
        String name = view.getName();
        String dob = view.getDOB();
        String mobile = view.getMobile();
        String userName = view.getUsername();
        String password = view.getPassword();

        if (name.isEmpty()) {
            view.showNameError(R.string.reg_name_error);
            return;
        }
        if (dob.isEmpty()) {
            view.showDOBError(R.string.reg_dob_error);
            return;
        }
        if (mobile.isEmpty()) {
            view.showMobileError(R.string.reg_mobile_error);
            return;
        }

        if (userName.isEmpty()) {
            view.showUsernameError(R.string.reg_username_error);
            return;
        }


        if (password.isEmpty()) {
            view.showPasswordError(R.string.reg_password_error);
            return;
        }




        boolean loginSucceeded = service.reg(name, dob,mobile,userName,password);
        if (loginSucceeded) {
            view.startMainActivity();
            return;
        }
        view.showRegError(R.string.reg_failed);
    }
}


