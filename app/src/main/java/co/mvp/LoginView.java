package co.mvp;

public interface LoginView {
    String getUsername();
    void showUsernameError(int resId);
    String getPassword();
    void showPasswordError(int resId);
    void startMainActivity();
    void startFPActivity();
    void startRegActivity();
    void startListActivity();
    void showLoginError(int resId);
}
