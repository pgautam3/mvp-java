package co.mvp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import static android.icu.text.DisplayContext.LENGTH_SHORT;

public class LoginActivity extends AppCompatActivity implements LoginView {
    private EditText usernameView;
    private EditText passwordView;
    private LoginPresenter presenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        usernameView = (EditText) findViewById(R.id.username);
        passwordView = (EditText) findViewById(R.id.password);
        presenter = new LoginPresenter(this, new LoginService());

    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public void onLoginClicked(View view) {
        presenter.onLoginClicked();
    }
    public void startListActivity(View view) {
        presenter.onListClicked();
    }

    public void onFPSubmitClicked(View view) {
        presenter.onFPClicked();
    }
    public void onRegistrationClicked(View view) {
        presenter.onRegClicked();
    }

    @Override
    public String getUsername() {
        return usernameView.getText().toString();
    }

    @Override
    public void showUsernameError(int resId) {
        usernameView.setError(getString(resId));
    }

    @Override
    public String getPassword() {
        return passwordView.getText().toString();
    }

    @Override
    public void showPasswordError(int resId) {
        passwordView.setError(getString(resId));
    }

    @Override
    public void startMainActivity() {
        new ActivityUtil(this).startMainActivity();
    }

    @Override
    public void startFPActivity() {
        new ActivityUtil(this).startForgotPasswowdActivity();


    }
    @Override
    public void startRegActivity() {
        new ActivityUtil(this).startregistrationActivity();


    }

    @Override
    public void startListActivity() {
        new ActivityUtil(this).startRecyclerViewActivity();


    }


    // onRegistrationClicked
    @Override
    public void showLoginError(int resId) {
        Toast.makeText(this, getString(resId), Toast.LENGTH_SHORT).show();

    }


}
