package co.mvp;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import co.mvp.forgotPassword.ForgotPasswordPresenter;
import co.mvp.forgotPassword.ForgotPasswordService;
import co.mvp.forgotPassword.ForgotPasswordView;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ForgotPasswordPresenterTest {

    @Mock
    private ForgotPasswordView view;
    @Mock
    private ForgotPasswordService service;
    private ForgotPasswordPresenter presenter;

    @Before
    public void setUp() throws Exception {
        presenter = new ForgotPasswordPresenter(view, service);
    }

    @Test
    public void shouldShowErrorMessageWhenUsernameIsEmpty() throws Exception {
        when(view.getUsername()).thenReturn("");
        presenter.onFPClicked();
        verify(view).showUsernameError(R.string.fp_username_error);
    }



    @Test
    public void shouldStartMainActivityWhenUsernameIsCorrect() throws Exception {
        when(view.getUsername()).thenReturn("james");
        when(service.ForgotPassword("james")).thenReturn(true);
        presenter.onFPClicked();
        verify(view).startMainActivity();
    }

    @Test
    public void shouldShowLoginErrorWhenUsernameAndPasswordAreInvalid() throws Exception {
        when(view.getUsername()).thenReturn("james");
        when(service.ForgotPassword("james")).thenReturn(false);
        presenter.onFPClicked();
        verify(view).showLoginError(R.string.fp_failed);
    }
}