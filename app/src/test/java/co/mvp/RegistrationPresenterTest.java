package co.mvp;

import android.widget.EditText;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mock;

import co.mvp.registartion.RegistrationPresenter;
import co.mvp.registartion.RegistrationService;
import co.mvp.registartion.RegistrationView;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class RegistrationPresenterTest {
    @Mock
    private RegistrationView view;
    @Mock
    private RegistrationService service;
    private RegistrationPresenter presenter;

    @Before
    public void setUp() throws Exception {
        presenter = new RegistrationPresenter(view, service);
    }
//    private EditText name;
//    private EditText dob;
//    private EditText username;
//    private EditText password;
//    private EditText mobile;

    @Test
    public void shouldShowErrorMessageWhenNameIsEmpty() throws Exception {
        when(view.getName()).thenReturn("");
        presenter.onRegistrationClicked();
        verify(view).showUsernameError(R.string.reg_name_error);
    }
    @Test
    public void shouldShowErrorMessageWhenDOBIsEmpty() throws Exception {
        when(view.getDOB()).thenReturn("");
        presenter.onRegistrationClicked();
        verify(view).showUsernameError(R.string.reg_dob_error);
    }

    @Test
    public void shouldShowErrorMessageWhenUsernameIsEmpty() throws Exception {
        when(view.getUsername()).thenReturn("");
        presenter.onRegistrationClicked();
        verify(view).showUsernameError(R.string.reg_username_error);
    }

    @Test
    public void shouldShowErrorMessageWhenPasswordIsEmpty() throws Exception {
        when(view.getUsername()).thenReturn("james");
        when(view.getPassword()).thenReturn("");
        presenter.onRegistrationClicked();

        verify(view).showPasswordError(R.string.reg_password_error);
    }

    @Test
    public void shouldStartMainActivityWhenCredientilsAreCorrect() throws Exception {
        when(view.getUsername()).thenReturn("james");
        when(view.getPassword()).thenReturn("bond");
        when(service.reg("james", "dob", "9999", "james99", "123456")).thenReturn(true);
        presenter.onRegistrationClicked();
        verify(view).startMainActivity();
    }

//    public boolean reg(String name, String dob, String mobile, String userName, String password) {
//        return "james".equals(name) && "dob".equals(dob)&&
//                "9999".equals(mobile) && "james99".equals(userName)&&
//                "123456".equals(password);
//    }


    @Test
    public void shouldShowLoginErrorWhenCredientilsAreInvalid() throws Exception {
        when(view.getUsername()).thenReturn("james");
        when(view.getPassword()).thenReturn("bond");
        when(service.reg("james", "dob", "9999", "james99", "123456")).thenReturn(false);

        presenter.onRegistrationClicked();
        verify(view).showRegError(R.string.reg_failed);
    }
}